
export LS_COLORS="${LS_COLORS}:di=32:"

export _JAVA_OPTIONS="-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true"
export DEVKITPRO=/opt/devkitpro
export DEVKITARM=$DEVKITPRO/devkitARM

export DERBY_INSTALL=/opt/Apache/db-derby-10.12.1.1-bin
export DERBY_OPTS="-Xms50m -Xmx50m"
export TOMCAT_HOME=/etc/tomcat8
export CLASSPATH=$DERBY_INSTALL/lib/derby.jar:$DERBY_INSTALL/lib/derbytools.jar:.

export PATH="$(ruby -e 'print Gem.user_dir')/bin:$PATH"


